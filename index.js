const textFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
const textLastName = document.querySelector("#txt-last-name");

textFirstName.addEventListener("input", updateFullName);
textLastName.addEventListener("input", updateFullName);

function updateFullName() {
	const firstName = textFirstName.value;
	const lastName = textLastName.value;

	const fullName = `${firstName} ${lastName}`

	spanFullName.innerHTML = fullName;
}
